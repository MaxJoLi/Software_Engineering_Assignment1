"""The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?"""

import math

var = 600851475143

def is_prime(x):
    searchflag = True
    isprime = True
    while searchflag == True:
      for y in xrange(2, x/2):
        if searchflag == False:
            break
        if x%y == 0:
            isprime = False
            break
      searchflag = False
    if isprime == False:
        return False
    else:
        return True


def prime_div(x):
    global prime_var
    if x > 1:
      for y in xrange(2, x+1, 1):
          if is_prime(y) and x%y == 0:
              prime_var = y
              prime_div(x/y)
              break


prime_div(var)
print prime_var